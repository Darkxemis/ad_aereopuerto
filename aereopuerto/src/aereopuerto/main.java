package aereopuerto;

import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


import controlador_aereopuerto.*;

public class main {
		
	 public static void main(String[] args) {
		 	
		 boolean salir = false;
		 int opcion = 0;
		 Scanner escaner = new Scanner(System.in);
		
		 ControladorAvion controlador_avion = new ControladorAvion();
		 ControladorAsiento controlador_asiento = new ControladorAsiento();
		 ControladorVuelo controlador_vuelo= new ControladorVuelo();
		 ControladorPasajeros controlador_pasajero = new ControladorPasajeros();
			 
		 
		 do {
			 System.out.println("Pulsa 1) para opciones de un avi�n");
			 System.out.println("Pulsa 2) para opciones de asiento");
			 System.out.println("Pulsa 3) para opciones de vuelo");
			 System.out.println("Pulsa 4) para opciones de pasajeros");
			 System.out.println("Pulsa 5) para filtrar las b�squedas");
			 System.out.println("Pulsa 6) para salir");
			 opcion = escaner.nextInt();
			 
			 switch (opcion) {
			 
		        case 1:
		        	
		        	Scanner escaner_avion = new Scanner(System.in);
		   		 	boolean terminar_avion = false;
		        	int opcion_busqueda_avion;
		        	
		        	do{
		        		
		        		 System.out.println("Pulsa 1) para a�adir avi�n");
		    			 System.out.println("Pulsa 2) para eliminar avion");
		    			 System.out.println("Pulsa 3) para modificar avi�n");
		    			 System.out.println("Pulsa 4) Atras");
		    			 opcion_busqueda_avion = escaner_avion.nextInt();
		    			 
/**************************************************AVION*********************************************************/
		    			 switch (opcion_busqueda_avion) {
		    			 
		    		        case 1:
		    		        	Scanner sca = new Scanner(System.in);
		    		        	String matricula,marca;
		    		        	int capacidad;
		    		    
		    		        	System.out.println("Introduce matricula");
		    		        	matricula = sca.nextLine();
		    		        	System.out.println("Introduce marca");
		    		        	marca = sca.nextLine();
		    		        	System.out.println("Introduce capacidad");
		    		        	capacidad = sca.nextInt();
		    		        	
		    		        	controlador_avion.IntroducirAvion(matricula,marca,capacidad); //Introduzco el avi�n
		    		        	controlador_avion.DevolverAviones(); // Devuelvo todos los aviones
		    		        	System.out.println("Se ha a�adido un nuevo avion a la base de datos");
		    		        	break;
		    		        case 2:
		    		        	Scanner sca2 = new Scanner(System.in);
		    		        	String matricula_eliminar = "";
		    		        	System.out.println("Introduce la matricula del avi�n que quieres eliminar");
		    		        	matricula_eliminar = sca2.nextLine();
		    		        	controlador_avion.EliminaAvion(matricula_eliminar);
		    		        	System.out.println("Avion con matricula: " + matricula_eliminar + " ha sido eliminado correctamente");
		    		        	break;
		    		        case 3:
		    		        	Scanner sca3 = new Scanner(System.in);
		    		        	String matricula_actualizar = "";
		    		        	System.out.println("Introduce la matricula del avi�n que quieres actualizar");
		    		        	matricula_actualizar = sca3.nextLine();
		    		        	System.out.println("Introduce nueva marca");
		    		        	marca = sca3.nextLine();
		    		        	System.out.println("Introduce nueva capacidad");
		    		        	capacidad = sca3.nextInt();
		    		        	Avion avion_actualizar = controlador_avion.DevolerAvionPorId(matricula_actualizar);
		    		        	avion_actualizar.setCapacidad(capacidad);
		    		        	avion_actualizar.setMarca(marca);
		    		        	controlador_avion.ActualizarAvion(avion_actualizar);
		    		        	System.out.println("Avion con matricula: " + matricula_actualizar + " se ha actualizado correctamente");
		    		        	break;
		    		        case 4:
		    		        	terminar_avion = true;
		    		        	break;
		    		        	
		    		        default:
		    		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		    		        	break;
		    			 }
		        		
		        	}while(!terminar_avion);
		        	
		        	break;
/**************************************************Asiento*********************************************************/		        	
		        case 2:
		        	
		        	Scanner escaner_asiento = new Scanner(System.in);
		   		 	boolean terminar_asiento = false;
		        	int opcion_busqueda_asiento;
		        	
		        	do{
		        		
		        		 System.out.println("Pulsa 1) para a�adir asiento");
		    			 System.out.println("Pulsa 2) para eliminar asiento");
		    			 System.out.println("Pulsa 3) Atras");
		    			 opcion_busqueda_asiento = escaner_asiento.nextInt();
		    			 

		    			 switch (opcion_busqueda_asiento) {
		    			 
		    		        case 1:
		    		        	Scanner scas = new Scanner(System.in);
		    		        	int numero = 0;
		    		        	System.out.println("Introduce el avi�n que quieres introducir los asientos");
		    		        	Avion avion_asientos =  controlador_avion.DevolerAvionPorId(scas.nextLine());
		    		        	
		    		        	while(numero != -1){
		    		        		System.out.println("Introduce asiento. Pulsa -1 para finalizar");
			    		        	numero = scas.nextInt();
			    		        	if (numero != -1){
			    		        		Asiento asiento = new Asiento(avion_asientos,numero);
			    		        		controlador_asiento.IntroducirAsiento(asiento);
			    		        		//avion_asientos.addAsiento(asiento);
			    		        	}
			    		        	controlador_avion.ActualizarAvion(avion_asientos);
		    		        	}
		    		        	
		    		        	System.out.println("Se ha a�adido todos los asientos");
		    		        	break;
		    		        case 2:
		    		        	
		    		        	Scanner scas2 = new Scanner(System.in);
		    		        	int numero_eliminar = 0;
		    		        	System.out.println("Introduce la matricula del avi�n que quieres eliminar");
		    		        	numero_eliminar = scas2.nextInt();
		    		        	controlador_asiento.EliminaAsiento(numero_eliminar);
		    		        	System.out.println("Asiento n�mero: " + numero_eliminar + " ha sido eliminado correctamente");
		    		        	break;
		    		        	
		    		    
		    		        case 3:
		    		        	terminar_asiento = true;
		    		        	break;
		    		        	
		    		        default:
		    		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		    		        	break;
		    			 }
		        		
		        	}while(!terminar_asiento);
		        	break;
/**************************************************Vuelo*********************************************************/
		        case 3:
		        	Scanner escaner_vuelo = new Scanner(System.in);
		   		 	boolean terminar_vuelo = false;
		        	int opcion_busqueda_vuelo;
		        	
		        	do{
		        		
		        		 System.out.println("Pulsa 1) para a�adir vuelo");
		    			 System.out.println("Pulsa 2) para eliminar vuelo");
		    			 System.out.println("Pulsa 3) para actualizar vuelo");
		    			 System.out.println("Pulsa 4) Atras");
		    			 opcion_busqueda_vuelo = escaner_vuelo.nextInt();
		    			 

		    			 switch (opcion_busqueda_vuelo) {
		    			 
		    		        case 1:
		    		        	Scanner scv = new Scanner(System.in);
		    		        	String matricula = "",nombre_vuelo = "";
		    		        	System.out.println("Introduce el avi�n para asociarselo al vuelo");
		    		        	Avion avion_vuelo =  controlador_avion.DevolerAvionPorId(scv.nextLine());
		    		        	System.out.println("Introduce el nombre del vuelo");
		    		        	nombre_vuelo = scv.nextLine();
			    		        controlador_vuelo.IntroducirVuelo(avion_vuelo,nombre_vuelo);		
			    		        controlador_avion.ActualizarAvion(avion_vuelo); 	
		    		        	System.out.println("Se ha a�adido el vuelo correctamente");
		    		        	break;
		    		        case 2:
		    		        	
		    		        	Scanner scv2 = new Scanner(System.in);
		    		        	String nombre_vuelo_eliminar = "";
		    		        	System.out.println("Introduce el nombre del vuelo que quieres eliminar");
		    		        	nombre_vuelo_eliminar = scv2.nextLine();
		    		        	controlador_vuelo.EliminaVuelo(nombre_vuelo_eliminar);
		    		        	System.out.println("Vuelo n�mero: " + nombre_vuelo_eliminar + " ha sido eliminado correctamente");
		    		        	break;
		    		        	
		    		    
		    		        case 3:
		    		        	Scanner scv3 = new Scanner(System.in);
		    		        	int id_vuelo_actualizar = 0;
		    		        	System.out.println("Introduce la matricula del vuelo que quieres actualizar");
		    		        	id_vuelo_actualizar = scv3.nextInt();
		    		        	System.out.println("Introduce el nuevo nombre");
		    		        	Vuelo vuelo_actualizar = controlador_vuelo.DevolerVueloPorId(id_vuelo_actualizar);
		    		        	vuelo_actualizar.setNombre(scv3.nextLine());
		    		        	controlador_vuelo.ActualizarVuelo(vuelo_actualizar);
		    		        	System.out.println("Avion con id: " + id_vuelo_actualizar + " se ha actualizado correctamente");
		    		        	break;
		    		        case 4:
		    		        	terminar_vuelo = true;
		    		        	break;
		    		        default:
		    		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		    		        	break;
		    			 }
		        		
		        	}while(!terminar_vuelo);
		        	break;
/**************************************************Pasajero*********************************************************/
		        case 4:
		        	Scanner escaner_pasajero = new Scanner(System.in);
		   		 	boolean terminar_pasajero = false;
		        	int opcion_busqueda_pasajero;
		        	
		        	do{
		        		
		        		 System.out.println("Pulsa 1) para a�adir pasajero");
		    			 System.out.println("Pulsa 2) para eliminar pasajero");
		    			 System.out.println("Pulsa 3) para actualizar pasajero");
		    			 System.out.println("Pulsa 4) Atras");
		    			 opcion_busqueda_pasajero = escaner_pasajero.nextInt();
		    			 

		    			 switch (opcion_busqueda_pasajero) {
		    			 
		    		        case 1:
		    		        	Scanner scp = new Scanner(System.in);
		    		        	String dni_pasajero = "",nombre_pasajero = "";
		    		        	System.out.println("Introduce el id vuelo que quieres asociar los pasajeros");
		    		        	Vuelo vuelo_pasajeros = controlador_vuelo.DevolerVueloPorId(scp.nextInt());
		    		        	
		    		        	while(dni_pasajero != "salir"){
		    		        		Scanner scpp = new Scanner(System.in);
		    		        		System.out.println("pon salir para cerrar");
		    		        		System.out.println("Introduce dni del pasajero");
		    		        		dni_pasajero = scpp.nextLine();
		    		        		System.out.println("Introduce nombre del pasajero");
			    		        	nombre_pasajero = scpp.nextLine();
			    		        	if (dni_pasajero.equals("salir") || nombre_pasajero.equals("salir")){
			    		        		break;
			    		        	}else{
			    		        		
			    		        		controlador_pasajero.IntroducirPasajero(vuelo_pasajeros, dni_pasajero, nombre_pasajero);
			    		        	}
			    		        	
		    		        	}
		    		        	controlador_vuelo.ActualizarVuelo(vuelo_pasajeros);
		    		        	System.out.println("Se ha a�adido todos los pasajeros");
		    		        	break;
		    		        case 2:
		    		        	
		    		        	Scanner scp2 = new Scanner(System.in);
		    		        	String dni_eliminar = "";
		    		        	System.out.println("Introduce el dni del pasajero que quieres eliminar");
		    		        	dni_eliminar = scp2.nextLine();
		    		        	controlador_pasajero.EliminaPasajero(dni_eliminar);
		    		        	System.out.println("pasajero con dni: " + dni_eliminar + " ha sido eliminado correctamente");
		    		        	break;
		    		        	
		    		    
		    		        case 3:
		    		        	Scanner scp3 = new Scanner(System.in);
		    		        	String dni_actualizar = "";
		    		        	System.out.println("Introduce el dni del pasajero que quieres actualizar");
		    		        	dni_actualizar = scp3.nextLine();
		    		        	System.out.println("Introduce el nuevo nombre");
		    		        	Pasajero pasajero_actualizar = controlador_pasajero.DevolerPasajeroPorDni(dni_actualizar);
		    		        	pasajero_actualizar.setNombre(scp3.nextLine());
		    		        	controlador_pasajero.ActualizarPasajero(pasajero_actualizar);
		    		        	System.out.println("Avion con id: " + dni_actualizar + " se ha actualizado correctamente");
		    		        	break;
		    		        case 4:
		    		        	terminar_pasajero = true;
		    		        	break;
		    		        default:
		    		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		    		        	break;
		    			 }
		        		
		        	}while(!terminar_pasajero);
		        	break;
		        case 5:
		        	Scanner escaner_busqueda = new Scanner(System.in);
		   		 	boolean terminar_busqueda = false;
		        	int opcion_busqueda;
		        	
		        	do{
		        		
		        		 System.out.println("Pulsa 1) para ver todos los aviones");
		    			 System.out.println("Pulsa 2) para buscar avion por matricula");
		    			 System.out.println("Pulsa 3) para ver todos los vuelos");
		    			 System.out.println("Pulsa 4) para buscar vuelo por id vuelo");
		    			 System.out.println("Pulsa 5) para ver todos los pasajeros");
		    			 System.out.println("Pulsa 6) para buscar un pasajero por DNI");
		    			 System.out.println("Pulsa 7) Atras");
		    			 opcion_busqueda = escaner_busqueda.nextInt();
		    			 

		    			 switch (opcion_busqueda) {
		    			 
		    		        case 1:
		    		        	controlador_avion.DevolverAviones();
		    		        	break;
		    		        case 2:
		    		        	String cadena;
		    		        	Scanner esc = new Scanner(System.in);
		    		        	String matricula_enviar;
		    		        	System.out.println("Introduce la matricula");
		    		        	matricula_enviar = esc.nextLine();
		    		        	Avion avion = controlador_avion.DevolerAvionPorId(matricula_enviar);
		    		        	//System.out.println(avion.toString());
		    		        	break;
		    		        case 3:
		    		        	controlador_vuelo.DevolverVuelo();
		    		        	break;
		    		        case 4:
		    		        	Scanner esc2 = new Scanner(System.in);
		    		        	int id_vuelo;
		    		        	System.out.println("Introduce el id_vuelo");
		    		        	id_vuelo = esc2.nextInt();
		    		        	Vuelo vuelo = controlador_vuelo.DevolerVueloPorId(id_vuelo);
		    		        	break;
		    		        case 5:
		    		        	controlador_pasajero.DevolverPasajeros();
		    		        	break;
		    		        case 6:
		    		        	Scanner esc3 = new Scanner(System.in);
		    		        	String dni_buscar;
		    		        	System.out.println("Introduce el DNI");
		    		        	dni_buscar = esc3.nextLine();
		    		        	Pasajero pasajero = controlador_pasajero.DevolerPasajeroPorDni(dni_buscar);
		    		        	break;
		    		        case 7:
		    		        	terminar_busqueda = true;
		    		        	break;
		    		        	
		    		        default:
		    		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		    		        	break;
		    			 }
		        		
		        	}while(!terminar_busqueda);
		        	
		        	
		        	break;
		        	
		        case 6:
		        	salir = true;
		        	break;
		        	
		        default:
		        	System.out.println("Error vuelve a pulsar una opci�n v�lida");
		        	break;
		 
			 }
			 
		 }while (!salir);
		 
		 System.out.println("Gracias por usar nuestra BD");

		}
}
