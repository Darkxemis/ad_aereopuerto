package aereopuerto;
// Generated 28-feb-2016 17:45:11 by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * Vuelo generated by hbm2java
 */
public class Vuelo implements java.io.Serializable {

	private Integer idVuelo;
	private Avion avion;
	private String nombre;
	private Set pasajeros = new HashSet(0);

	public Vuelo() {
	}

	public Vuelo(Avion avion, String nombre) {
		this.avion = avion;
		this.nombre = nombre;
	}

	public Vuelo(Avion avion, String nombre, Set pasajeros) {
		this.avion = avion;
		this.nombre = nombre;
		this.pasajeros = pasajeros;
	}

	public Integer getIdVuelo() {
		return this.idVuelo;
	}

	public void setIdVuelo(Integer idVuelo) {
		this.idVuelo = idVuelo;
	}

	public Avion getAvion() {
		return this.avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set getPasajeros() {
		return this.pasajeros;
	}

	public void setPasajeros(Set pasajeros) {
		this.pasajeros = pasajeros;
	}
	
	public String toString(){
		String cadena = "Avi�n: " + this.avion.getMarca() + "\t" + "Nombre vuelo: " + this.nombre + "\n" + "______________________________________________" + "\n";
		for(Object object : this.pasajeros) {
		   Pasajero pasajero = (Pasajero) object;
		   cadena += "DNI: ------>" + pasajero.getDni() + "\n";
		   cadena += "Nombre: ------>" + pasajero.getNombre() + "\n";
		   cadena += "........................." + "\n";
		  // System.out.println("Asiento: ------>" + asiento.getNumero());
		   } 
		return cadena;
	}

}
