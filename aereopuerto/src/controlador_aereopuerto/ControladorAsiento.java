package controlador_aereopuerto;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import aereopuerto.Asiento;
import aereopuerto.HibernateUtil;

public class ControladorAsiento {
	
	public void DevolverAsiento(){
		 
		List<Asiento> asiento = new ArrayList<Asiento>(); 
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            asiento = session.createQuery("from Asiento").list();
            for(int i = 0; i<asiento.size();i++){
            	Asiento asient;
            	asient = asiento.get(i);
            	System.out.println("Todos los Asientos: " + asient.getNumero());
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

	}
	
	 public void IntroducirAsiento(Asiento asiento){
		
		 Transaction trns = null;
	     Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            //Asiento asiento = new Asiento(numero);
	            session.save(asiento);
	            session.getTransaction().commit();
	            System.out.println("Se ha introducido correctamente el asiento");
	            
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }	  
			
	 }
	 
	 public void EliminaAsiento(int numero) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        Asiento asiento = null;
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Asiento where numero = :numero";
	            Query query = session.createQuery(queryString);
	            query.setInteger("numero", numero);
	            asiento = (Asiento) query.uniqueResult();
	            //Avion avion = (Avion) session.load(Avion.class, new String(matricula));
	            session.delete(asiento);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
	 
}
