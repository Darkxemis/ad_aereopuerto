package controlador_aereopuerto;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import aereopuerto.*;

public class ControladorAvion {
	
	public void DevolverAviones(){
		 
		List<Avion> aviones = new ArrayList<Avion>(); 
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String cadena;
            aviones = session.createQuery("from Avion").list();
            for(int i = 0; i<aviones.size();i++){
            	Avion avion;
            	avion = aviones.get(i);
            	cadena = avion.toString();
            	System.out.println(cadena);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

	}
	 public Avion DevolerAvionPorId(String matricula) {
		 	Avion avion = null;
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	        	String cadena;
	            trns = session.beginTransaction();
	            String queryString = "from Avion where matricula = :matricula";
	            Query query = session.createQuery(queryString);
	            query.setString("matricula", matricula);
	            avion = (Avion) query.uniqueResult();
	            
	            cadena = avion.toString();
            	System.out.println(cadena);

	        } catch (RuntimeException e) {
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return avion;
	    }
	 public void IntroducirAvion(String matricula,String marca,int capacidad){
		
		 Transaction trns = null;
	     Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            Avion avion = new Avion(matricula,marca,capacidad);
	            session.save(avion);
	            session.getTransaction().commit();
	            System.out.println("Se ha introducido correctamente el avi�n");
	            
	            
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }	  
			
	 }
	 
	 public void EliminaAvion(String matricula) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        Avion avion = null;
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Avion where matricula = :matricula";
	            Query query = session.createQuery(queryString);
	            query.setString("matricula", matricula);
	            avion = (Avion) query.uniqueResult();
	            //Avion avion = (Avion) session.load(Avion.class, new String(matricula));
	            session.delete(avion);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
	 
	 public void ActualizarAvion(Avion avion) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.update(avion);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
}
