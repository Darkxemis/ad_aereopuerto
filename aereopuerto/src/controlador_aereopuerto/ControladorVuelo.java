package controlador_aereopuerto;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import aereopuerto.Avion;
import aereopuerto.HibernateUtil;
import aereopuerto.Vuelo;

public class ControladorVuelo {
	public void DevolverVuelo(){
		 
		List<Vuelo> vuelos = new ArrayList<Vuelo>(); 
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String cadena;
            vuelos = session.createQuery("from Vuelo").list();
            for(int i = 0; i<vuelos.size();i++){
            	Vuelo vuelo;
            	vuelo = vuelos.get(i);
            	cadena = vuelo.toString();
            	System.out.println(cadena);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

	}
	 public Vuelo DevolerVueloPorId(int id_vuelo) {
		 	Vuelo vuelo = null;
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	        	String cadena;
	            trns = session.beginTransaction();
	            String queryString = "from Vuelo where id_vuelo = :id_vuelo";
	            Query query = session.createQuery(queryString);
	            query.setInteger("id_vuelo", id_vuelo);
	            vuelo = (Vuelo) query.uniqueResult();
	            cadena = vuelo.toString();
            	System.out.println(cadena);
	            
	        } catch (RuntimeException e) {
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return vuelo;
	    }
	 public void IntroducirVuelo(Avion avion,String nombre){
		
		 Transaction trns = null;
	     Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            Vuelo vuelo = new Vuelo(avion,nombre);
	            session.save(vuelo);
	            session.getTransaction().commit();
	            System.out.println("Se ha introducido correctamente el avi�n");
	            
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }	  
			
	 }
	 
	 public void EliminaVuelo(String nombre) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        Vuelo vuelo = null;
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Vuelo where nombre = :nombre";
	            Query query = session.createQuery(queryString);
	            query.setString("nombre", nombre);
	            vuelo = (Vuelo) query.uniqueResult();
	            //Avion avion = (Avion) session.load(Avion.class, new String(matricula));
	            session.delete(vuelo);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
	 
	 public void ActualizarVuelo(Vuelo vuelo) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.update(vuelo);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
}
