package controlador_aereopuerto;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import aereopuerto.Avion;
import aereopuerto.HibernateUtil;
import aereopuerto.Pasajero;
import aereopuerto.Vuelo;

public class ControladorPasajeros {
	public void DevolverPasajeros(){
		 
		List<Pasajero> pasajeros = new ArrayList<Pasajero>(); 
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String cadena;
            pasajeros = session.createQuery("from Pasajero").list();
            for(int i = 0; i<pasajeros.size();i++){
            	Pasajero pasajero;
            	pasajero = pasajeros.get(i);
            	cadena = pasajero.toString();
            	System.out.println(cadena);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

	}
	 public Pasajero DevolerPasajeroPorDni(String dni) {
		 	Pasajero pasajero = null;
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Pasajero where dni = :dni";
	            Query query = session.createQuery(queryString);
	            query.setString("dni", dni);
	            pasajero = (Pasajero) query.uniqueResult();
            	System.out.println(pasajero.toString());
            	
	        } catch (RuntimeException e) {
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	        return pasajero;
	    }
	 public void IntroducirPasajero(Vuelo vuelo,String dni,String nombre){
		
		 Transaction trns = null;
	     Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            Pasajero pasajero = new Pasajero(vuelo,dni,nombre);
	            session.save(pasajero);
	            session.getTransaction().commit();
	            System.out.println("Se ha introducido correctamente el pasajero");
	            
	            List<Avion> aviones = new ArrayList<Avion>();
	            aviones = session.createQuery("from Avion").list();
	            
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }	  
			
	 }
	 
	 public void EliminaPasajero(String dni) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        Pasajero pasajero = null;
	        try {
	            trns = session.beginTransaction();
	            String queryString = "from Pasajero where dni = :dni";
	            Query query = session.createQuery(queryString);
	            query.setString("dni", dni);
	            pasajero = (Pasajero) query.uniqueResult();
	            //Avion avion = (Avion) session.load(Avion.class, new String(matricula));
	            session.delete(pasajero);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
	 
	 public void ActualizarPasajero(Pasajero pasajero) {
	        Transaction trns = null;
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        try {
	            trns = session.beginTransaction();
	            session.update(pasajero);
	            session.getTransaction().commit();
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.flush();
	            session.close();
	        }
	    }
}
